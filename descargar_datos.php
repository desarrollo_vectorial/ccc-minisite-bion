<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');


    $conexion = @mysql_connect ("localhost", "ccc_usr", "PjabeqxmyaVxD0px");
    mysql_select_db ("ccc_db", $conexion);
    mysql_query("SET NAMES utf8");
    mysql_query("SET CHARACTER SET utf8");
    $sql = "SELECT * FROM lp_concurso_arbitraje_comercial";
    $resultado = mysql_query ($sql, $conexion) or die (mysql_error ());
    $registros = mysql_num_rows ($resultado);

    /*$conexion = @mysql_connect ("localhost", "root", "root");
    mysql_select_db ("ccc_2017", $conexion);
    mysql_query("SET NAMES utf8");
    mysql_query("SET CHARACTER SET utf8");
    $sql = "SELECT * FROM lp_concurso_arbitraje_comercial";
    $resultado = mysql_query ($sql, $conexion) or die (mysql_error ());
    $registros = mysql_num_rows ($resultado);*/

    

    if ($registros > 0) {
        /** Include PHPExcel */
        require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Andrés Felipe Yepes Ortega")
            ->setLastModifiedBy("Andrés Felipe")
            ->setTitle("Registros Base de datos 2017")
            ->setSubject("Registros Base de datos 2017")
            ->setDescription("Test developer Andrés Felipe Yepes O.")
            ->setKeywords("Base de datos")
            ->setCategory("Registers");


        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:V1')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('263f95');

        $objPHPExcel->getActiveSheet()->getStyle("A1:V1")->getFont()->setSize(16);
        $objPHPExcel->getActiveSheet()->getStyle("A:V")->getFont()->setSize(16);


        for($col = 'A'; $col !== 'V'; $col++) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        //set up the style in an array
        $style = array('font' => array('size' => 18,'bold' => true,'color' => array('rgb' => 'ffffff')));

        //apply the style on column A row 1 to Column B row 1
        $objPHPExcel->getActiveSheet()->getStyle('A1:V1')->applyFromArray($style);

        $i = 2;    
        while ($registro = mysql_fetch_object ($resultado)) {

            $objPHPExcel->setActiveSheetIndex(0)
                ->SetCellValue('A1', 'Nombre Universidad que Representa')
                ->SetCellValue('B1', 'Ciudad Institución')
                ->SetCellValue('C1', 'Dirección Institución')
                ->SetCellValue('D1', 'Teléfono Fijo Institución')
                ->SetCellValue('E1', 'Nombres y Apellidos Jugador 1')
                ->SetCellValue('F1', 'Nombres y Apellidos Jugador 2')
                ->SetCellValue('G1', 'Nombres y Apellidos Jugador 3')
                ->SetCellValue('H1', 'Nombres y Apellidos Jugador 4')
                ->SetCellValue('I1', 'Nombres y Apellidos Jugador 5')
                ->SetCellValue('J1', 'Cédula Jugador 1')
                ->SetCellValue('K1', 'Cédula Jugador 2')
                ->SetCellValue('L1', 'Cédula Jugador 3')
                ->SetCellValue('M1', 'Cédula Jugador 4')
                ->SetCellValue('N1', 'Cédula Jugador 5')
                ->SetCellValue('O1', 'Nombres y Apellidos Entrenador')
                ->SetCellValue('P1', 'Cédula Entrenador')
                ->SetCellValue('Q1', 'Nombres y Apellidos Contacto')
                ->SetCellValue('R1', 'Correo Contacto')
                ->SetCellValue('S1', 'Teléfono y Fijo del Contacto')
                ->SetCellValue('T1', 'Enlaces de los Documentos')
                ->SetCellValue('U1', 'Autorización de Datos')
                ->SetCellValue('V1', 'Fecha')
                ->setCellValue('A'.$i, $registro->name_universidad_representa)
                ->setCellValue('B'.$i, $registro->ciudad_insitucion)
                ->setCellValue('C'.$i, $registro->direccion_institucion)
                ->setCellValue('D'.$i, $registro->telefono_fijo_institucion)
                ->setCellValue('E'.$i, $registro->nombres_apellidos_jugador_1)
                ->setCellValue('F'.$i, $registro->nombres_apellidos_jugador_2)
                ->setCellValue('G'.$i, $registro->nombres_apellidos_jugador_3)
                ->setCellValue('H'.$i, $registro->nombres_apellidos_jugador_4)
                ->setCellValue('I'.$i, $registro->nombres_apellidos_jugador_5)
                ->setCellValue('J'.$i, $registro->cedula_jugador_1)
                ->setCellValue('K'.$i, $registro->cedula_jugador_2)
                ->setCellValue('L'.$i, $registro->cedula_jugador_3)
                ->setCellValue('M'.$i, $registro->cedula_jugador_4)
                ->setCellValue('N'.$i, $registro->cedula_jugador_5)
                ->setCellValue('O'.$i, $registro->nombres_apellidos_entrenador)
                ->setCellValue('P'.$i, $registro->cedula_entrenador)
                ->setCellValue('Q'.$i, $registro->nombres_apellidos_contacto)
                ->setCellValue('R'.$i, $registro->correo_contacto)
                ->setCellValue('S'.$i, $registro->tel_fijo_contacto)
                ->setCellValue('T'.$i, $registro->enlaces_documentos)
                ->setCellValue('U'.$i, $registro->autorizacion_datos)
                ->setCellValue('V'.$i, $registro->fecha);

            $i++;
        }
    }

    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('Simple');


    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);

    $fechadescarga = date("Y/F/d H:i:s"); 
    /*header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="registros-exponegocios-'.$fechadescarga.'.xlsx"');
    header('Cache-Control: max-age=0');

    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    exit;*/

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
/*header('Content-Disposition: attachment;filename="01simple.xlsx"');*/
header('Content-Disposition: attachment;filename="concurso-arbitraje-comercial-registros-'.$fechadescarga.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;