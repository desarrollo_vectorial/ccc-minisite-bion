<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'dev_ccc_bion');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '=3!58^}QvzZsSr-!<aN9MU](-zxNph/J5YL@;>=e1O)`e`pK^[E$)5QX8JLtwQrV');
define('SECURE_AUTH_KEY', 'Ul69VFMx-vB+Q3]P<0=ZmqE8X7S{?:V I{RVbDt:aA?x36m%,;42s$_aBZHXt$4/');
define('LOGGED_IN_KEY', 'C7{Ef54P,nqs-DL~{NPs]My5/rrK/0Te4{KI7T!m)4dD+8UMIh/NPkl9XLOSA.:P');
define('NONCE_KEY', '~6&&NaK<R JO%vqW6D8u9X[D[/b<#0020Mlp%OqO4>xeC&q*v{)a=Tx3reb~P)D!');
define('AUTH_SALT', 'r9~MT`+cuh>F-<pmBvC=08|Vi?UE7.=@=?Z>]07M&7;$oB;1i*dzgVNYA(~N36c0');
define('SECURE_AUTH_SALT', 'wLI@`#P9wmzn*<aW`>ec;&NzNTb,?O1Ou!:mNg{1NN$S;&cWP%0gTD&LS{^:nd[4');
define('LOGGED_IN_SALT', 'oy&9^(4zE;K]^[[,9C+b_3U&yRnDj4p>kMPe;u_8rwI,nFj{D>qFKX,L=UF!]k=8');
define('NONCE_SALT', 'eOk{5xk*b&T8Ldde0to>i8^]mc*c*n cUR_#gj8:|y2A{1Z&X{X:Dz:NOJpi:{--');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'bion_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

