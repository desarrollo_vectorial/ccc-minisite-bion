<?php
ob_start();
    error_reporting(E_ALL); 
    ini_set( 'display_errors','1');
    date_default_timezone_set("America/Bogota");
    // CAPTURAMOS LOS DATOS ENVIADOS POR AJAX

    include('class.uploader.php');

    /*Variables*/
    $nombre = $_POST['nombre'];
    $tema = $_POST['tema'];
    $institucion = $_POST['institucion'];
    $ciudad = $_POST['ciudad'];
    $email = $_POST['email'];
    $telefono = $_POST['telefono'];
    $fecha = date("j/F/Y");

    $dirsave = "documentos-bion/".$email;
    $uploader = new Uploader();
    $data = $uploader->upload($_FILES['files'], array(
        'limit' => 10, //Maximum Limit of files. {null, Number}
        'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
        'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
        'required' => false, //Minimum one file is required for upload {Boolean}
        'uploadDir' => $dirsave, //Upload directory {String}
        'title' => array('name'), //New file name {null, String, Array} *please read documentation in README.md
        'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
        'replace' => true, //Replace the file if it already exists {Boolean}
        'perms' => null, //Uploaded file permisions {null, Number}
        'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
        'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
        'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
        'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
        'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
        'onRemove' => 'onFilesRemoveCallback' //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
    ));


    if($data['isComplete']){
        // CONFIGURAMOS EL ENVIO DE LOS ARCHIVOS ADJUNTOS
        $files2 = $data['data'];

        $urlsDocumentos ="";
        foreach ( $files2['files'] as $como ) {
            $urlsDocumentos .='http://ccc.minisite.bion.local/'.$como.', ';
        }

        echo $urlsDocumentos;

        //Data Base
        // Abrimos la conexion a la base de datos

        /*$servername = "localhost";
        $username = "ccc_usr";
        $password = "PjabeqxmyaVxD0px";
        $dbname = "ccc_db";*/

        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "dev_ccc_bion";

        try {
            $params = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, $params);

            $sql = "INSERT INTO `bion_forms_landing` (`nombre`, `tema`, `institucion`, `ciudad`, `email`, `telefono`,`doc`, `autorizacion`, `fecha`) VALUES ('".$nombre."', '".$tema."', '".$institucion."', '".$ciudad."', '".$email."', '".$telefono."','".$urlsDocumentos."', '".$autorizacion."', '".$fecha."')";

            $conn->exec($sql);

            }
        catch(PDOException $e)
            {
            echo $sql . "<br>" . $e->getMessage();
            }

        $conn = null;

        header ("Location: index.php");

        exit(); //terminamos la ejecución del script.

    }

    if($data['hasErrors']){
        $errors = $data['errors'];
        print_r($errors);
    }

    
ob_end_flush();
?>