<div class="como-llegar text-center">
    <p>Cómo llegar</p>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <?php echo do_shortcode('[wpgmza id="1"]'); ?>
        </div>
        <div class="col-md-6">
            <p class="pointer-como"><strong>Valle del Pacífico,</strong><br>Centro de Eventos</p>
            <div class="inscribete-box text-center">
                <a href="#" class="btn btn-lg btn-inscribe">Inscribete aquí</a>
            </div>
            <div class="mas-info">
                <h4>Para más información</h4>
                <p class="mail">bion@ccc.org.co</p>
                <p class="phone">(572) 886 13 00 Ext 459 - 389</p>
            </div>
        </div>
    </div>
</div>