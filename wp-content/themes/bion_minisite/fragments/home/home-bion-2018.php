<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h1><strong>BI-ON</strong> 2018</h1>
            <p><strong>BI-ON es un evento académico en formato de Congreso con muestra comercial,</strong> con el objetivo de generar conexiones para facilitar el acceso a conocimiento especializado sobre el estado actual y tendencias del mercado de Bioenergía en el mundo y en Colombia, nuevas tecnologías, avances regulatorios y nuevos modelos de negocio.</p>
            <p><strong>BI-ON 2018 es organizado por la Iniciativa Cluster de Bioenergía de la Cámara de Comercio de Cali,</strong>en la que participan las empresas relacionadas con los procesos de generación de biogás, bioelectricidad y biocombustibles de la región.</p>
        </div>
        <div class="col-md-6">
            <img src="<?php bloginfo('template_url') ?>/assets/images/bion-2018.png" alt="" class="img-responsive">
        </div>
    </div>
</div>