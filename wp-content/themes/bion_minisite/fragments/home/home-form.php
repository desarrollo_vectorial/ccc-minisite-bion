<div class="form form-material">
    <form method="post" id="form-patrocinador" class="formPrincipal" action="send.php" enctype="multipart/form-data">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center" id="first-column">
                    <h2>Formulario de inscripción</h2>
                </div>
                <div class="col-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <input type="text" name="nombre" id="nombre" />
                        <label class="control-label" for="input">
                            Nombre y apellido del contacto para la convocatoria
                        </label>
                        <i class="bar"></i>
                    </div>
                    <div class="form-group">
                        <input type="text" name="tema" id="tema" />
                        <label class="control-label" for="input">
                            Tema de investigación
                        </label>
                        <i class="bar"></i>
                    </div>
                    <div class="form-group">
                        <div class="form-group material-file-inputs">
                            <h4>Adjunte aqui su documento de participación</h4>
                            <div class="file-drop-area">
                                <span class="fake-btn">Seleccionar archivo</span>
                                <span class="file-msg js-set-number">No has seleccionado ningún archivo</span>
                                <input class="file-input" type="file" id="files" name="files[]" accept=".gif,.jpg,.jpeg,.png,.pdf,.docx">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <input type="text" name="institucion" id="institucion" />
                        <label class="control-label" for="input">
                            Institución
                        </label>
                        <i class="bar"></i>
                    </div>
                    <div class="form-group">
                        <input type="text" name="ciudad" id="ciudad" />
                        <label class="control-label" for="input">
                            Ciudad,pais
                        </label>
                        <i class="bar"></i>
                    </div>
                    <div class="form-group">
                        <input type="text" name="email" id="email" />
                        <label class="control-label" for="input">
                            E-mail(s) de contacto
                        </label>
                        <i class="bar"></i>
                    </div>
                    <div class="form-group">
                        <input type="text" name="telefono" id="telefono" />
                        <label class="control-label" for="input">
                            Teléfono de contacto
                        </label>
                        <i class="bar"></i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="checkbox">
                                <label class="checkbox-inline">
                                    <input type="checkbox" class="tasks-list-cb" id="autorizacion" name="autorizacion" value="aceptados">
                                    <span class="tasks-list-mark"></span>
                                    <span class="tasks-list-desc"><a class="terminos-enlace" href="http://www.ccc.org.co/inc/uploads/2017/03/habeas-data.pdf" target="_blank">Autorizo el tratamiento de datos personales*</a></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="button-container">
                                <button class="button" type="submit"><span>Enviar</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--end .row-->
        </div><!--end .row-->
    </form><!--end form-->
</div><!--end formmaterial-->