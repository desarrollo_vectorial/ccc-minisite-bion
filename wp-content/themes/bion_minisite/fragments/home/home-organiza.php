<div class="organiza-title text-center">
    <p>Organiza</p>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <img src="<?php bloginfo('template_url') ?>/assets/images/organiza-logo.png" alt="">
        </div>
        <div class="col-md-4">
            <p>Aliados estratégicos:</p>
            <div class="col-md-12 icons-aliados">
                <img src="<?php bloginfo('template_url') ?>/assets/images/el-valle-esta-en-vos-logo.png" alt="">
            </div>
        </div>
        <div class="col-md-4">
            <p>Apoya:</p>
            <div class="col-xs-6 icons-apoya">
                <img src="<?php bloginfo('template_url') ?>/assets/images/sector-azucarero-logo.png" alt="">
            </div>
            <div class="col-xs-6 icons-apoya">
                <img src="<?php bloginfo('template_url') ?>/assets/images/CRC-logo.png" alt="">
            </div>
            <div class="col-xs-6 icons-apoya">
                <img src="<?php bloginfo('template_url') ?>/assets/images/fenavi-logo.png" alt="">
            </div>
            <div class="col-xs-6 icons-apoya">
                <img src="<?php bloginfo('template_url') ?>/assets/images/invest-pacific-logo.png" alt="">
            </div>
        </div>
        <div class="col-md-4">
            <p>Medio oficial:</p>
            <div class="col-md-12 icons-medio">
                <img src="<?php bloginfo('template_url') ?>/assets/images/el-valle-esta-en-vos-logo.png" alt="">
            </div>
        </div>
    </div>
</div>