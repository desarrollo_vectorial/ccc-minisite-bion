<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p><strong>BI-ON 2018,</strong> 2do Congreso Nacional de Bioenergía</p>
            </div>
            <div class="row footer-nav">
                <div class="col-md-2 col-md-offset-1">
                    <ul>
                        <li><a href="#">Bi-on 2018</a></li>
                        <li><a href="#">Agenda académica</a></li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul>
                        <li><a href="#">Cali, ciudad sede</a></li>
                        <li><a href="#">Iniciativa Cluster</a></li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul>
                        <li><a href="#">Zona posters</a></li>
                        <li><a href="#">Visibilice su marca</a></li>
                    </ul>
                </div>
                <div class="col-md-2">
                    <ul>
                        <li><a href="#">Términos de referencia</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-md-offset-1">
                    <img src="<?php bloginfo('template_url') ?>/assets/images/camara-logo-footer.png" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid powered">
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="http://vectorial.co/" target="_blank"><img src="<?php bloginfo('template_url') ?>/assets/images/powered-by-vectorial.png" alt=""></a>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>