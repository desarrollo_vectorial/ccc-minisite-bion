<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?> <?php bloginfo('description'); ?></title>
    <!-- AGREGA TUS ESTILOS -->
    <link href="<?php bloginfo('template_url') ?>/assets/css/main.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_url') ?>/assets/images/favicon.png"/>
    <!-- AGREGA TUS SCRIPTS -->
    <script src="<?php bloginfo('template_url') ?>/assets/js/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url') ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url') ?>/assets/js/my-functions.js"></script>
    <?php
    wp_enqueue_script('jquery');
    wp_head();
    ?>
</head>
<body <?php body_class(); ?>>
<header>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img class="img-responsive" src="<?php bloginfo('template_url') ?>/assets/images/logo-menu-ppal.png" alt=""></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="rrss">
                    <ul>
                        <li>
                        <span class="fa-stack">
                            <i class="fa fa-square-o fa-stack-2x"></i>
                            <i class="fa fa-facebook fa-stack-1x"></i>
                        </span>
                        </li>
                        <li>
                        <span class="fa-stack">
                            <i class="fa fa-square-o fa-stack-2x"></i>
                            <i class="fa fa-twitter fa-stack-1x"></i>
                        </span>
                        </li>
                        <li>
                        <span class="fa-stack">
                            <i class="fa fa-square-o fa-stack-2x"></i>
                            <i class="fa fa-youtube-play fa-stack-1x"></i>
                        </span>
                        </li>
                    </ul>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="/"><i class="fa fa-home"></i></a></li>
                    <li><a href="#">Bi-on 2018</a></li>
                    <li><a href="#">Agenda académica</a></li>
                    <li><a href="#">Iniciativa clúster</a></li>
                    <li><a href="#">Zona de pósters</a></li>
                    <li><a href="#">Visibilice su marca</a></li>
                    <li><a href="#">Inscripciones</a></li>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>