<?php
get_header();
wp_head();
?>

<div class="header-banner" style="background-image: url(<?php bloginfo('template_url') ?>/assets/images/header.png);">
    <?php include get_template_directory() . "/fragments/home/home-ubicacion.php"; ?>
</div>
<div class="bion-2018">
    <?php include get_template_directory() . "/fragments/home/home-bion-2018.php"; ?>
</div>
<div class="video" style="background-image: url(<?php bloginfo('template_url') ?>/assets/images/video.png);">
    <?php include get_template_directory() . "/fragments/home/home-video-modal.php"; ?>
</div>
<div class="locacion">
    <?php include get_template_directory() . "/fragments/home/home-como-llegar.php"; ?>
</div>
<div class="organiza">
    <?php include get_template_directory() . "/fragments/home/home-organiza.php"; ?>
</div>

<?php
wp_footer();
get_footer();
?>

