<?php

/* Template Name: TEST TEMPLATE */

get_header();
wp_head();

?>

<?php
if (have_posts()) {
    while (have_posts()) {
        the_post();
        echo the_content();
    }
}
?>
    <div class="locacion">
        <?php include get_template_directory() . "/fragments/home/home-como-llegar.php"; ?>
    </div>
    <div class="organiza">
        <?php include get_template_directory() . "/fragments/home/home-organiza.php"; ?>
    </div>

<?php

wp_footer();
get_footer();

?>